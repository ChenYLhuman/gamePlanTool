﻿#ifndef H_MY__MAINWINDOW_H__H
#define	H_MY__MAINWINDOW_H__H
#include <QMainWindow>
#include <QListWidget>
#include <qtabwidget.h>
#include <qstring.h>
#include <QList>
#include <qlabel.h>
#include <qwidget.h>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <QAction>
#include <QMenu>
#include <qmenubar.h>
#include <qstatusbar.h>
#include <QtCore/QtCore>
#include <QHBoxLayout>
#include <QTextEdit>
#include "AlgorithmPage.h"
#include "LoadDataPage.h"
#include "PropertyPage.h"
#include "MsgPage.h"
#include "MyListWidgetItem.h"
#include <qcombobox.h>
#include "CommunicationPage.h"
#include "MenuPage.h"

namespace widgetUI {
	class MainWindow;
}

namespace appInstances {
	class MainWindow;
}

class widgetUI::MainWindow : public QMainWindow {
Q_OBJECT ;
public:
	friend class appInstances::MainWindow;
public:
	MainWindow(QWidget* parent = nullptr);
	void setFileAuthor(const QString& iconPath, const QString& authorName);
	void initAppControl( );
	void initPermissing( );
	void initStatusMenu(QMenu& status_edit_menu);
	bool eventFilter(QObject* watched, QEvent* event) override;
	int addPage(widgetUI::ATabPageBase* page);
	virtual ~MainWindow( );
	void clearList( );
	void additemToList(MyListWidgetItem* item);
private: // 响应状态栏
	void statusEditTrigger(bool checked = false);
private: // 响应 QTabWidget
	// 被改变
	void currentChanged(int index);
	// 单机页面
	void tabBarClicked(int index);
	// 双击页面
	void tabBarDoubleClicked(int index);
private: // 控件
	widgetUI::MsgPage* msgPage = Q_NULLPTR;
	QPushButton* creatorPersonBtn;
	QVBoxLayout* personLayout;
	static widgetUI::MsgPage* msgPageInstance(widgetUI::MsgPage* msgPage = Q_NULLPTR, bool set = false);
	widgetUI::LoadDataPage* loadDataPage = Q_NULLPTR;
	static widgetUI::LoadDataPage* loadDataPageInstance(widgetUI::LoadDataPage* msgPage = Q_NULLPTR, bool set = false);
	widgetUI::PropertyPage* propertyPage = Q_NULLPTR;
	static widgetUI::PropertyPage* propertyPageInstance(widgetUI::PropertyPage* msgPage = Q_NULLPTR, bool set = false);
	widgetUI::AlgorithmPage* algorithmPage = Q_NULLPTR;
	static widgetUI::AlgorithmPage* algorithmInstance(widgetUI::AlgorithmPage* msgPage = Q_NULLPTR, bool set = false);
	widgetUI::CommunicationPage* commPage = Q_NULLPTR;
	static widgetUI::CommunicationPage* commPageInstance(widgetUI::CommunicationPage* msgPage = Q_NULLPTR, bool set = false);
	widgetUI::MenuPage* menuPage = Q_NULLPTR;
	static widgetUI::MenuPage* menuPageInstance(widgetUI::MenuPage* msgPage = Q_NULLPTR, bool set = false);
public:
	static widgetUI::MsgPage* getMsgPage( );
	static widgetUI::LoadDataPage* getLoadDataPage( );
	static widgetUI::PropertyPage* getPropertyPage( );
	static widgetUI::AlgorithmPage* getAlgorithmPage( );
	static widgetUI::CommunicationPage* getCommPage( );
	static widgetUI::MenuPage* getMenuPage( );
private:
	QWidget* mainWidget = Q_NULLPTR;
	QHBoxLayout* mainLayout = Q_NULLPTR;
	QListWidget* objListWidget = Q_NULLPTR;
	QTabWidget* tabWidget = Q_NULLPTR;
	QLabel* fileUserIcon = Q_NULLPTR;
	QLabel* fileUserName = Q_NULLPTR;
	QLabel* spacer = Q_NULLPTR;
public:
	QLabel* editStatus;
	QStatusBar* status_bar;
	QLineEdit* person;
private:
	void initConnection( );
	void toolStatusBarClick( );
	void creatorPersonClick(bool clicked);
	void objListItemClicked(QListWidgetItem* item);
signals: // tab 事件
	// 被改变
	void tabWidgetCurrentChanged(int index, QWidget* sendSignalObj, QTabWidget* tabBody, widgetUI::ATabPageBase* page);
	// 单击页面
	void tabWidgetTabBarClicked(int index, QWidget* sendSignalObj, QTabWidget* tabBody, widgetUI::ATabPageBase* page);
	// 双击页面
	void tabWidgetTabBarDoubleClicked(int index, QWidget* sendSignalObj, QTabWidget* tabBody, widgetUI::ATabPageBase* page);
	// 被点击创建按钮
	void creatorPerson(MainWindow* eventObj, QPushButton* btn, bool Clicked);
signals: // list 事件
	void listItemClicked(MyListWidgetItem* item);
signals: // 编辑标签事件
	void statusClick(QWidget* widget);
};
#endif // MAINWINDOW_H
