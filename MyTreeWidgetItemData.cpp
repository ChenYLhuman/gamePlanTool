﻿#include "MyTreeWidgetItemData.h"
#include <qdebug.h>

MyTreeWidgetItemData::MyTreeWidgetItemData(QTreeWidget* treeview, const QStringList& strings, QString fullPath, int type) : QTreeWidgetItem(treeview, strings, type) {
	this->fullPath = fullPath;
}

MyTreeWidgetItemData::MyTreeWidgetItemData(QTreeWidgetItem* parent, const QStringList& strings, QString fullPath, int type): QTreeWidgetItem(parent, strings, type) {
	this->fullPath = fullPath;
}

MyTreeWidgetItemData::MyTreeWidgetItemData(const QStringList& strings, QString fullPath, int type) : QTreeWidgetItem(strings, type) {
	this->fullPath = fullPath;
}

MyTreeWidgetItemData::~MyTreeWidgetItemData( ) {}
