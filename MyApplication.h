﻿#ifndef H_MY__MYAPPLICATION_H__H
#define	H_MY__MYAPPLICATION_H__H
#include <QApplication>
#include "ISigAbstract.h"
#include <QMutexLocker>
#include <qheaderview.h>
#include "AlgorithmPage.h"
#include "PropertyPage.h"
#include "MsgPage.h"
#include "LoadDataPage.h"
#include "mainwindow.h"
#include "MyApplication.h"
#include "SappInstance.h"
class SigInstance;

namespace widgetUI {
	class MainWindow;
}

class MyApplication : public QApplication {
Q_OBJECT;
private:
	// 处理页空间
	void processingTabPageWidget(widgetUI::MainWindow* mainWindow);
	// 处理加载页面
	void processingLoadPage(const widgetUI::MainWindow* main_window);
public:
	SigInstance* m_sigInstance = Q_NULLPTR;
	void setWinProcessing(widgetUI::MainWindow* win);
	QMenu* notifyToMouseEvent(QWidget* mouseOnWidgetObj, QEvent::Type eventType, const QPoint& mousePos, const QPoint& oldMousePos);
	bool notify(QObject*, QEvent*) override;
	MyApplication(int& argc, char** argv, int i = ApplicationFlags);
	~MyApplication( );
public:
public:
	class objName {
	public:
		static const char* loadPageTreeWdigetHeadViewObjName;
	};
};
#endif // H_MY__MYAPPLICATION_H__H
