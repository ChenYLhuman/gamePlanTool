﻿#ifndef H_MY__ACOMMUNICATIONITEM_H__H
#define	H_MY__ACOMMUNICATIONITEM_H__H
#include <QStringList>
#include <QFrame>
#include <QVBoxLayout>
#include "dXml.h"

namespace widgetUI {
	class AAlgorithmItem;
}

class widgetUI::AAlgorithmItem : public QWidget {
Q_OBJECT;
public:
	AAlgorithmItem(QWidget* parent = Q_NULLPTR
		, const Qt::WindowFlags& f = Qt::WindowFlags( )): QWidget(parent, f) { }

	virtual ~AAlgorithmItem( ) {}

	// 获取 item 名称
	virtual const QString getItemName( ) = 0;
	// 获取 xml 映射节点
	virtual const dXml::XmlDataDoc getXmlData( ) const = 0;
	// 布局
	virtual const QVBoxLayout* mainLayout( ) = 0;
	// 中心 widget
	virtual const QWidget* mainWidget( ) =0;
	// 获取图标
	virtual const QIcon getIcon( ) =0;
};
#endif // H_MY__COMMUNICATIONITEM_H__H
