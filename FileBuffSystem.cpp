﻿#include "FileBuffSystem.h"
appInstances::FileBuffSystem appInstances::FileBuffSystem::fileInstance{ };

appInstances::FileBuffSystem::FileBuffSystem( ) {
	dirPathBody = new QVector<QString>;
	filePathody = new QVector<QString>;
	xmlMap = new QHash<QString, dXml::XmlDataDoc>;
	// 一些默认存档
	QString ipTag;
	QString piData;
	dXml::XmlDataDoc defautDoc;
	dXml::readXmlFile(&defautDoc, ipTag, piData, defaultXmlStting);
	insertMapUnity(defaultXmlStting, defautDoc);
}

bool appInstances::FileBuffSystem::haveFileKey(const QString& filePath) {
	return xmlMap->contains(filePath);
}

void appInstances::FileBuffSystem::setCurrentDir(const QString& fullPath) {
	currentDirFullPath = fullPath;
}

void appInstances::FileBuffSystem::addFileDir(const QString& fullPath) {
	dirPathBody->append(fullPath);
}

void appInstances::FileBuffSystem::removeFileDir(const QString& fullPath) {
	dirPathBody->removeOne(fullPath);
}

void appInstances::FileBuffSystem::clearFileDir( ) {
	dirPathBody->clear( );
}

const QString& appInstances::FileBuffSystem::getCurrentDir( ) {
	return currentDirFullPath;
}

const QVector<QString> appInstances::FileBuffSystem::getDir(const QString& key) {
	QVector<QString> result;
	QVector<QString>::iterator iterator = dirPathBody->begin( );
	QVector<QString>::iterator end = dirPathBody->end( );
	while( iterator != end ) {
		if( ( *iterator ).contains(key, Qt::CaseInsensitive) )
			result.append(*iterator);
		++iterator;
	}
	return result;
}

void appInstances::FileBuffSystem::setCurrentFilePath(const QString& fullPath) {
	currentFileFullPath = fullPath;
}

void appInstances::FileBuffSystem::addFilePath(const QString& fullPath) {
	filePathody->append(fullPath);
}

void appInstances::FileBuffSystem::removeFilePath(const QString& fullPath) {
	filePathody->removeOne(fullPath);
}

void appInstances::FileBuffSystem::clearFilePath( ) {
	filePathody->clear( );
}

const QString& appInstances::FileBuffSystem::getCurrentFilePath( ) {
	return currentFileFullPath;
}

const QVector<QString> appInstances::FileBuffSystem::getFilePath(QString& key) {
	QVector<QString> result;
	QVector<QString>::iterator iterator = filePathody->begin( );
	QVector<QString>::iterator end = filePathody->end( );
	while( iterator != end ) {
		if( ( *iterator ).contains(key, Qt::CaseInsensitive) )
			result.append(*iterator);
		++iterator;
	}
	return result;
}

void appInstances::FileBuffSystem::insertMapUnity(const QString& fileFullPath
	, const dXml::XmlDataDoc& xmlDocObj
	, int blend) {
	if( blend != 0 ) {
		dXml::XmlDataDoc doc = xmlMap->value(fileFullPath);
		doc = dXml::xmlBlend(doc, xmlDocObj, blend);
	} else {
		xmlMap->insert(fileFullPath, xmlDocObj);
	}
}

void appInstances::FileBuffSystem::removeMapUnity(const QString& key) {
	xmlMap->remove(key);
}

void appInstances::FileBuffSystem::clearMapUnitys( ) {
	xmlMap->clear( );
}

const dXml::XmlDataDoc appInstances::FileBuffSystem::getDocValue(const QString& key) {
	dXml::XmlDataDoc doc = xmlMap->value(key);
	if( !xmlMap->contains(key) ) {
		if( dXml::readXmlFile(&doc, key) )
			xmlMap->insert(key, doc);
	}
	return doc;
}

const QList<QString> appInstances::FileBuffSystem::getKeys( ) {
	return xmlMap->keys( );
}

QHash<QString, dXml::XmlDataDoc> appInstances::FileBuffSystem::cloneMap( ) {
	QHash<QString, dXml::XmlDataDoc> result;
	QHash<QString, dXml::XmlDataDoc>::iterator iterator = xmlMap->begin( );
	QHash<QString, dXml::XmlDataDoc>::iterator end = xmlMap->end( );
	while( iterator != end ) {
		dXml::XmlDataDoc doc = iterator.value( );
		const QString key = iterator.key( );
		result.insert(key, doc);
		++iterator;
	}
	return result;
}

appInstances::FileBuffSystem::~FileBuffSystem( ) {
	if( this->dirPathBody != Q_NULLPTR )
		delete dirPathBody;
	dirPathBody = Q_NULLPTR;
	if( this->filePathody != Q_NULLPTR )
		delete filePathody;
	filePathody = Q_NULLPTR;
	delete xmlMap;
	//if( this->currentInfo != Q_NULLPTR )
	//	delete currentInfo;
	//currentInfo = Q_NULLPTR;
}
