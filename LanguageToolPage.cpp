﻿#include "LanguageToolPage.h"
#include <iso646.h>
#include <qicon.h>
#include <qstring.h>
#include <qobject.h>
#include <qapplication.h>
#include <qstyle.h>
#include <QVBoxLayout>
#include <QDebug>
#include "XmlDataUnit.h"

widgetUI::LanguageToolPage::LanguageToolPage(QWidget* paren): AToolBoxPage{ paren } {
	// 设置图标
	QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	// 创建主要布局
	mainLayout = new QVBoxLayout(this);
	mainLayout->setContentsMargins(0, 0, 0, 0);
	mainLayout->setSpacing(0);
	controlGroupBox = new QGroupBox("language", this);
	mainLayout->addWidget(controlGroupBox);
	controlGroupLayout = new QVBoxLayout( );
	controlGroupLayout->setContentsMargins(0, 0, 0, 0);
	controlGroupLayout->setSpacing(0);
	controlGroupBox->setLayout(controlGroupLayout);
	xmlLanguageUnit = new dXml::XmlDataUnit;
	map = new QHash<QRadioButton*, QString>;
}

widgetUI::LanguageToolPage::~LanguageToolPage( ) {
	if( xmlLanguageUnit != Q_NULLPTR )
		delete xmlLanguageUnit;
	xmlLanguageUnit = Q_NULLPTR;
}

void widgetUI::LanguageToolPage::setTitle(QString& str) {
	controlGroupBox->setTitle(str);
}

void widgetUI::LanguageToolPage::activation(QObject* obj, int index) {}

dXml::XmlDataUnit widgetUI::LanguageToolPage::getXmlUnityToCloneObj( ) {
	return *xmlLanguageUnit;
}

QString widgetUI::LanguageToolPage::getName( ) {
	return objPageName;
}

QIcon widgetUI::LanguageToolPage::getIcon( ) {
	return objPageIcon;
}

void widgetUI::LanguageToolPage::setName(QString& name) {
	this->objPageName = name;
}

void widgetUI::LanguageToolPage::setIcon(QIcon& icon) {
	this->objPageIcon = icon;
}

QList<QWidget*> widgetUI::LanguageToolPage::setXmlUnityDom(dXml::XmlDataUnit* mode) {
	QList<QWidget*> result;
	if( mode->nodeName( ) != "language" )
		return result;
	clear( );
	xmlLanguageUnit->setName("language");
	appendItems(result, mode);
	return result;
}

void widgetUI::LanguageToolPage::sortWidget(bool order) {}

void widgetUI::LanguageToolPage::clear( ) {
	xmlLanguageUnit->clear( );
	const QList<QObject*> list = controlGroupBox->children(  );
	for( auto& obj : list )
		controlGroupLayout->removeWidget(qobject_cast<QWidget*>(obj));
}

void widgetUI::LanguageToolPage::appendItems(QList<QWidget*>& result, dXml::XmlDataUnit*& mode) {
	xmlLanguageUnit->appendAttrs(*mode->getAttrs( ));
	QList<dXml::XmlDataUnit*>* list = mode->getChildrens( );
	QList<dXml::XmlDataUnit*>::iterator iterator = list->begin( );
	QList<dXml::XmlDataUnit*>::iterator end = list->end( );
	for( ;iterator != end;++iterator ) {
		dXml::XmlDataUnit* unit = *iterator;
		xmlLanguageUnit->appendChildren(unit);
		auto&& msg = unit->nodeName( );
		auto&& path = unit->getText( );
		QRadioButton* radioButton = new QRadioButton(msg, this);
		controlGroupLayout->addWidget(radioButton);
		ConnectRadioButton(radioButton);
		map->insert(radioButton, path);
		result.append(radioButton);
	}
}

QList<QWidget*> widgetUI::LanguageToolPage::setXmlUnityDoms(QList<dXml::XmlDataUnit*>& modes) {
	QList<QWidget*> result;
	if( modes.isEmpty( ) )
		return result;
	xmlLanguageUnit->clear( );
	xmlLanguageUnit->setName("language");
	QList<dXml::XmlDataUnit*>::iterator iterator = modes.begin( );
	QList<dXml::XmlDataUnit*>::iterator end = modes.end( );
	for( ;iterator != end;++iterator ) {
		appendItems(result, *iterator);
	}
	return result;
}

QWidget* widgetUI::LanguageToolPage::addItem(QIcon icon, const QString& msg, const QString& Data, QHash<QString, QString>* attrMap, bool isChicked) {
	QRadioButton* radioButton = qobject_cast<QRadioButton*>(addItem(msg, Data, attrMap, isChicked));
	radioButton->setIcon(icon);
	return radioButton;
}

bool widgetUI::LanguageToolPage::synchXmlUnit( ) {
	QHash<QRadioButton*, QString>::iterator iterator = map->begin( );
	QHash<QRadioButton*, QString>::iterator end = map->end( );
	QString name = xmlLanguageUnit->nodeName( );
	xmlLanguageUnit->clear( );
	xmlLanguageUnit->setName(name);
	for( ;iterator != end;++iterator ) {
		QRadioButton* radioButton = iterator.key( );
		QString nodeText = iterator.value( );
		dXml::XmlDataUnit* unit = new dXml::XmlDataUnit;
		unit->setText(nodeText);
		unit->setName(radioButton->text( ));
		xmlLanguageUnit->appendChildren(unit);
	}
	return true;
}

QWidget* widgetUI::LanguageToolPage::addItem(const QString& msg, const QString& Data, QHash<QString, QString>* attrMap, bool isChicked) {
	QRadioButton* radioButton = qobject_cast<QRadioButton*>(addItem(msg, Data, attrMap));
	radioButton->setChecked(isChicked);
	return radioButton;
}

QWidget* widgetUI::LanguageToolPage::addItem(const QString& msg, const QString& Data, QHash<QString, QString>* attrMap) {
	QRadioButton* radioButton = new QRadioButton(msg, this);
	controlGroupLayout->addWidget(radioButton);
	ConnectRadioButton(radioButton);
	map->insert(radioButton, Data);
	dXml::XmlDataUnit* unita = new dXml::XmlDataUnit(msg);
	unita->setText(Data);
	xmlLanguageUnit->appendChildren(unita);
	if( attrMap != Q_NULLPTR )
		unita->appendAttrs(*attrMap);
	return radioButton;
}

void widgetUI::LanguageToolPage::ConnectRadioButton(QRadioButton* signalsObj) {
	connect(signalsObj, &QRadioButton::toggled, this, &LanguageToolPage::toggledItem);
}

void widgetUI::LanguageToolPage::toggledItem(bool checked) {
	QRadioButton* radioButton = qobject_cast<QRadioButton*>(sender( ));

	// todo:测试单选
	//qDebug( ) << map->value(radioButton);
	emit toggled(this, *radioButton, checked);
}
