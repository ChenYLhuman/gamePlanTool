﻿#ifndef H_MY__SINGLETONLOADTHREAD_H__H
#define	H_MY__SINGLETONLOADTHREAD_H__H
#include "LoadDiscDirInfoThread.h"

namespace appInstances {
	class LoadDiscDirInfoThread;
}

class appInstances::LoadDiscDirInfoThread {
public:
	friend class lThread::LoadDiscDirInfoThread;
	static  lThread::LoadDiscDirInfoThread* GetSigInstanceInstance(const bool release = false, const SigInstance* sigInstance = Q_NULLPTR, QWidget* paren = Q_NULLPTR);
};
#endif // H_MY__SINGLETONLOADTHREAD_H__H
