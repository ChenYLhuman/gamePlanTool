﻿#include "AlgorithmPage.h"
#include <qapplication.h>
#include <qstyle.h>
#include <QIcon>
#include <qdebug.h>
#include <QToolBox>
#include <QtCore/QtCore>
#include "AlgorithmItem.h"
#include "AlgorithmTopItemWidget.h"
#include "FileBuffSystem.h"
using namespace widgetUI;

void AlgorithmPage::activation(QObject* obj
	, int index
	, EVENTFLAG status) {
	qDebug( ) << getName( );
}

void AlgorithmPage::initUi( ) {
	toolBox = new QToolBox(this);
	ui.mainLayout->addWidget(toolBox);
	toolBox->layout( )->setSpacing(0);
	toolBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	toolBox->setAttribute(Qt::WA_DeleteOnClose);
}

/*void AlgorithmPage::appendItemWidget(AAlgorithmItem* item) {
	
	toolBox->addItem(item, item->getIcon( ), item->getItemName( ));
}*/

void AlgorithmPage::initGlobalAlgroithmItem( ) {
	globalAlgroithmItem = new AlgorithmTopItemWidget(this);
	toolBox->addItem(globalAlgroithmItem, globalAlgroithmItem->getIcon( ), globalAlgroithmItem->getItemName( ));
}

void AlgorithmPage::initConnect( ) {
	connect(ui.ctreaorItem, &QPushButton::clicked, this, &AlgorithmPage::appExpreeItem);
	void (QComboBox::* asignal)(int) = &QComboBox::currentIndexChanged;
	//connect(ui.algorithmItemList, asignal, this, &AlgorithmPage::listIndexChanged);
	connect(ui.saveStting, &QPushButton::clicked, this, &AlgorithmPage::saveData);
}

void AlgorithmPage::blenderFileBuffDoc(dXml::XmlDataDoc doc) {
	QList<dXml::XmlDataUnit*> list = doc.findChildrens(unit->nodeName( ));
	for( int listindex = 0;listindex < list.size( );++listindex ) {
		dXml::XmlDataUnit* unit = list.at(listindex);
		unit->setParen(Q_NULLPTR);
		delete unit;
	}
	doc.appendChildren(new dXml::XmlDataUnit(*unit));
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(currentPath, doc);
}
//
//void AlgorithmPage::listIndexChanged(int index) {
//	auto&& fileFullPath = ui.algorithmItemList->currentData( ).toString( );
//	if( !currentPath.isEmpty( ) ) {
//		dXml::XmlDataDoc doc = appInstances::FileBuffSystem::fileInstance.getDocValue(currentPath);
//		blenderFileBuffDoc(doc);
//	}
//	currentPath = fileFullPath;
//}

void AlgorithmPage::appExpreeItem(bool checked) {
	globalAlgroithmItem->addItem(ui.name->text( ), ui.expressionEdit->text( ));
}

void AlgorithmPage::saveData(bool checked) {
	auto xmlDataDoc = globalAlgroithmItem->getXmlData( );
	QString dir = appInstances::FileBuffSystem::fileInstance.getCurrentDir( );
	//auto&& iterator = pageItemMap.begin( );
	//auto&& end = pageItemMap.end( );
	dXml::XmlDataUnit* children = new dXml::XmlDataUnit(getName( ));
	//xmlDataDoc.appendChildren(children);
	//for( ;iterator != end;++iterator ) {
	//	AAlgorithmItem* item = iterator.value( );
	//	auto&& itemDoc = item->getXmlData( );
	//	QList<dXml::XmlDataUnit*>* list = itemDoc.getChildrens( );
	//	children->appendChildrens(*list);
	//}

	children->appendChildrens(*xmlDataDoc.getChildrens( ));
	xmlDataDoc.appendChildren(children);
	QDir pathDir;
	QFileInfo info = QFileInfo(dir);
	const QString saveTargetDir = QDir::currentPath( ) + "/appSetting/";
	if( !QFile::exists(saveTargetDir) )
		pathDir.mkpath(saveTargetDir);
	auto fileFull = saveTargetDir + "/" + qApp->applicationName( ) + ".xmlAlgo";
	if( QFile::exists(fileFull) ) {
		const QString tarDIR = saveTargetDir + "/bak/";
		pathDir.mkpath(tarDIR);
		QFile::rename(fileFull, tarDIR + QDateTime::currentDateTime( ).toString("yyyy.MM.dd.hh.mm.ss.zzz") + '.' + qApp->applicationName( ) + ".xmlAlgo");
	}
	xmlDataDoc.save(fileFull);
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileFull, xmlDataDoc);
}

/*void AlgorithmPage::setDoc(dXml::XmlDataDoc& doc) {
}*/

void AlgorithmPage::initXmlSetting( ) {
	// todo : 从文件中读取
	auto fileFull = QDir::currentPath( ) + "/appSetting/" + "/" + qApp->applicationName( ) + ".xmlAlgo";
	dXml::XmlDataDoc doc;
	dXml::readXmlFile(&doc, fileFull);
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileFull, doc);
	QList<dXml::XmlDataUnit*> list = doc.findChildrens(getName( ));
	QList<dXml::XmlDataUnit*>::iterator iterator = list.begin( );
	QList<dXml::XmlDataUnit*>::iterator end = list.end( );
	for( ;iterator != end;++iterator ) {
		dXml::XmlDataUnit* unitchildrens = *iterator;
		QList<dXml::XmlDataUnit*> childrens = *unitchildrens->getChildrens( );
		for( auto& unit : childrens ) {
			globalAlgroithmItem->addItem(unit->nodeName( ), unit->getText( ));
		}
	}
}

AlgorithmPage::AlgorithmPage(QWidget* parent) : ATabPageBase(parent) {
	ui.setupUi(this);
	initUi( );
	setObjectName(getName( ));
	initGlobalAlgroithmItem( );
	initXmlSetting( );
	initConnect( );
	unit = new dXml::XmlDataUnit(getName( ));
}

AlgorithmPage::~AlgorithmPage( ) { }

void AlgorithmPage::selectItem(const QString& itemData) { }

/*
void AlgorithmPage::appendPage(QString name
	, QString xmlFileFullPath) {
	if( pageItemMap.contains(xmlFileFullPath) )
		return;
	QString targetName = xmlFileFullPath.append("/").append(name).append(".xml");
	AlgorithmItem* algorithmItem = new AlgorithmItem(name, targetName);
	pageItemMap.insert(targetName, algorithmItem);
	ui.algorithmItemList->addItem(name, targetName);
	toolBox->addItem(algorithmItem, algorithmItem->getIcon( ), algorithmItem->getItemName( ));
}
*/

/*void AlgorithmPage::appendExpress(const QString& name
	, const QString& express) {
	const QString dir = appInstances::FileBuffSystem::fileInstance.getCurrentDir(  );
	globalAlgroithmItem->addItem(name, express);
}*/

QString AlgorithmPage::getName( ) {
	static QString objName = "算法";
	return objName;
}

QIcon AlgorithmPage::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

/*
void AlgorithmPage::clear( ) {
	int count = toolBox->count( );
	for( int index = 1;index < count;++index ) {
		toolBox->removeItem(index);
	}
	auto&& iterator = pageItemMap.begin( );
	auto&& end = pageItemMap.end( );
	for( ;iterator != end;++iterator ) {
		auto object = *iterator;
		if( object != Q_NULLPTR && object != globalAlgroithmItem ) {
			object->setParent(Q_NULLPTR);
			delete object;
		}
	}
	/*pageItemMap.clear( );
	ui.algorithmItemList->clear( );
	if( !currentPath.isEmpty( ) ) {
		dXml::XmlDataDoc doc = appInstances::FileBuffSystem::fileInstance.getDocValue(currentPath);
		blenderFileBuffDoc(doc);
	}
	currentPath = "";#1#
	// auto children = toolBox->layout(  )->children(  );
}*/

/*
AlgorithmPage* AlgorithmPage::getInstance( ) {
	static AlgorithmPage instance;
	return &instance;
}
*/
