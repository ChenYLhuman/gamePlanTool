﻿#include "singletonLoadThread.h"
#include "LoadDiscDirInfoThread.h"

lThread::LoadDiscDirInfoThread* appInstances::LoadDiscDirInfoThread::GetSigInstanceInstance(const bool release
	, const SigInstance* sigInstance
	, QWidget* paren) {
	static lThread::LoadDiscDirInfoThread* instance = Q_NULLPTR;
	static bool isRentry = false;
	if( release && instance != Q_NULLPTR ) {
		instance->releaseResources( );
		delete instance;
		instance = Q_NULLPTR;
		return instance;
	}
	if( instance == Q_NULLPTR && !release && paren && sigInstance ) {
		instance = new lThread::LoadDiscDirInfoThread(sigInstance, paren);
	}
	return instance;
}
