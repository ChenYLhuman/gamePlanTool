﻿#ifndef H_MY__MATHPAIR_H__H
#define	H_MY__MATHPAIR_H__H
#include "dXml.h"
#include "MathExpressResolve.h"
#include "MathExpressBody.h"

class dXml::MathExpress::MathPairUnit {
private:
	friend class dXml::MathExpress::MathManage;
	// 配对
	MathExpressBody* pBody;
	MathExpressResolve* pResolve;
public:
	// 排序子
	void* pSortKey;

	MathPairUnit(MathExpressBody* body = Q_NULLPTR
		, MathExpressResolve* resolve = Q_NULLPTR
		, void* sortKey = Q_NULLPTR) : pBody{ body },
		pResolve{ resolve },
		pSortKey{ sortKey } { }

	~MathPairUnit( ) {
		delete pResolve;
		delete pBody;
	}
};
#endif // H_MY__MATHPAIR_H__H
