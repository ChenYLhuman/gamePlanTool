﻿#include <qdebug.h>
#include <QTreeWidgetItem>
#include <QTime>
#include <functional>
#include <QStringList>
#include <mutex>
#include <QList>
#include <qcolor.h>
#include "MyTreeWidgetItemData.h"
#include "singletonLoadThread.h"
#include "SigInstance.h"
#include "LoadDiscDirInfoThread.h"

void lThread::LoadDiscDirInfoThread::foreachFilePath(QTreeWidgetItem* paren_item
	, QDir paren_path
	, QString& time_fromatl) {
	if( paren_path.isEmpty( ) )
		return;
	QList<QFileInfo> pathInfos = paren_path.entryInfoList( );
	for( auto& fileInfo : pathInfos ) {
		QString name = fileInfo.fileName( );
		if( name == '.' || name == ".." || name == "bak" )
			continue;
		QMutexLocker locker(threadMutex( ));
		if( *threadRunStatus( ) )
			return;
		locker.unlock( );
		bool isFile = fileInfo.isFile( );
		if( isFile ) {
			QString suffix = fileInfo.suffix( );
			if( suffix != "xml" )
				continue;
		}
		const char* string = ( isFile ? "文件" : "文件夹" );
		QTreeWidgetItem* children = new MyTreeWidgetItemData(QStringList( ) << name << string << fileInfo.created( ).toString(time_fromatl), fileInfo.absoluteFilePath( ));
		paren_item->addChild(children);
		QDir absolute_dir = fileInfo.absoluteFilePath( );
		foreachFilePath(children, absolute_dir, time_fromatl);
	}
}

QMutex* lThread::LoadDiscDirInfoThread::threadMutex( ) {
	static QMutex q_mutex;
	return &q_mutex;
}

int* lThread::LoadDiscDirInfoThread::threadRunStatus(int* status) {
	static int runStatus = 0;
	if( status != Q_NULLPTR )
		runStatus = *status;
	return &runStatus;
}

void lThread::LoadDiscDirInfoThread::releaseResources( ) {
	// 释放资源
	int status = 0xf1;
	while( true ) {
		QMutexLocker locker(threadMutex( ));
		if( !this->isRunning( ) )
			break;
		if( !( *threadRunStatus( ) ) )
			threadRunStatus(&status);
	}
	this->quit( );
	// qDebug( ) << "结束线程任务\t(" << parenPath << ")," << this;
}

lThread::LoadDiscDirInfoThread::~LoadDiscDirInfoThread( ) {
	releaseResources( );
}

lThread::LoadDiscDirInfoThread::LoadDiscDirInfoThread(const SigInstance* sigInstance
	, QWidget* paren): QThread(paren) {
	this->m_sigInstance = sigInstance;
	connect(this, &LoadDiscDirInfoThread::overThreadTask, m_sigInstance, &SigInstance::overLoadDataThread, Qt::UniqueConnection);
	connect(this, &LoadDiscDirInfoThread::startThreadTask, m_sigInstance, &SigInstance::startLoadDataThread, Qt::UniqueConnection);
	connect(this, &LoadDiscDirInfoThread::execute, m_sigInstance, &SigInstance::execute, Qt::UniqueConnection);
	// 允许发送信号
	widgetUI::LoadDataPage* loadDataPageM = widgetUI::MainWindow::getLoadDataPage( );
	void ( widgetUI::LoadDataPage::* asignal)(QTreeWidgetItem*
		, QTreeWidgetItem*) = &widgetUI::LoadDataPage::currentItemChanged;
	void ( SigInstance::* slot)(QTreeWidgetItem*
		, QTreeWidgetItem*) = &SigInstance::currentItemChanged;
	connect(loadDataPageM, asignal, m_sigInstance, slot, Qt::UniqueConnection);
}

void lThread::LoadDiscDirInfoThread::run( ) {
	emit startThreadTask(this);
	runTask( );
}

void lThread::LoadDiscDirInfoThread::runTask( ) {
	static QMutex _qmutex;
	QMutexLocker locker(&_qmutex);
	int status = 0x0;
	threadRunStatus(&status);
	// 添加列表根
	QList<MyTreeWidgetItemData*>* treeItem = new QList<MyTreeWidgetItemData*>;
	QDir currPath = QDir(this->parenPath);
	QList<QFileInfo> file_infos = currPath.entryInfoList( );
	locker.unlock( );
	int taskNumber = file_infos.size( );
	int currentTaskIndex = 0;
	if( taskNumber )
		for( QFileInfo& info : file_infos ) {
			locker.relock( );
			QString fileFullPath = info.absoluteFilePath( );
			status = *threadRunStatus( );
			emit execute(taskNumber, ++currentTaskIndex, fileFullPath, status);
			if( status )
				break;
			locker.unlock( );
			QString name = info.fileName( );
			if( name == '.' || name == ".." || name == "bak" )
				continue;
			//qDebug( ) << name;
			QString fullPath = info.absoluteFilePath( );
			bool isFile = info.isFile( );
			if( isFile ) {
				QString suffix = info.suffix( );
				if( suffix != "xml" )
					continue;
			}
			const char* string = ( isFile ? "文件" : "文件夹" );
			QStringList string_list = QStringList( ) << name << string << info.created( ).toString(this->timeFromat);
			MyTreeWidgetItemData* children = new MyTreeWidgetItemData(string_list, fullPath);
			treeItem->append(children);
			foreachFilePath(children, fileFullPath, this->timeFromat);
		}
	status = 0x1;
	threadRunStatus(&status);
	emit overThreadTask(this, this->treeWidget, this->parenPath, this->timeFromat, treeItem, status);
}

void lThread::LoadDiscDirInfoThread::stopTask( ) {

	// 释放资源
	this->releaseResources( );
}

void lThread::LoadDiscDirInfoThread::startTask( ) {
	// 释放资源
	this->releaseResources( );
	this->start( );
}

void lThread::LoadDiscDirInfoThread::setTask(QTreeWidget* treeWidget
	, QString* foreachPath
	, QString* timeFormat) {
	this->treeWidget = treeWidget;
	this->parenPath = *foreachPath;
	this->timeFromat = *timeFormat;
}
