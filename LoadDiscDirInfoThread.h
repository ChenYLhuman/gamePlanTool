﻿#ifndef H_MY__LOADDATA_H__H
#define	H_MY__LOADDATA_H__H
#include <qstring.h>
#include <QList>
#include <QFile>
#include <qobject.h>
#include <QDir>
#include <string>
#include <QFileInfo>
#include <qtreewidgetitemiterator.h>
#include <QMutexLocker>
#include <qmutex.h>
#include <qrunnable.h>
#include "SappInstance.h"
#include "MyTreeWidgetItemData.h"
#include <QThread>

namespace lThread {
	class LoadDiscDirInfoThread;
}

class lThread::LoadDiscDirInfoThread : public QThread {
Q_OBJECT;
private:
	QThread* loadThreadObj = Q_NULLPTR;
	QString parenPath = "";
	QString timeFromat = "";
	QTreeWidget* treeWidget = Q_NULLPTR;
	const SigInstance* m_sigInstance = Q_NULLPTR;
	void foreachFilePath(QTreeWidgetItem* paren_item, QDir paren_path, QString& time_fromatl);
	QMutex* threadMutex( );
	int* threadRunStatus(int* status = Q_NULLPTR);
	void releaseResources( );
protected:
	void run( ) override;
public:
	void startTask( );
	void runTask( );
	void stopTask( );
	friend class appInstances::LoadDiscDirInfoThread;
	void setTask(QTreeWidget* treeWidget, QString* foreachPath, QString* timeFormat);
	~LoadDiscDirInfoThread( );
	LoadDiscDirInfoThread(const SigInstance* sigInstance, QWidget* paren = Q_NULLPTR);
signals:
	// 任务完成
	void overThreadTask(LoadDiscDirInfoThread* threadObj, QTreeWidget* treeWidget, QString rootFilePath, QString timeFromat, QList<MyTreeWidgetItemData*>* treeList, int runCode);
	// 任务开始
	void startThreadTask(LoadDiscDirInfoThread* obj);
	// 任务进度
	void execute(int taskNumber, int taskIndex, const QString& pathFull, const int status);
};
#endif // H_MY__LOADDATA_H__H
