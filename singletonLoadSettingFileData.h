﻿#ifndef H_MY__SINGLETONLOADSETTINGFILEDATA_H__H
#define	H_MY__SINGLETONLOADSETTINGFILEDATA_H__H
#include "LoadSettingFileData.h"

namespace appInstances {
	class LoadSettingFileData;
}

class appInstances::LoadSettingFileData {
public:
public:
	friend class lThread::LoadSettingFileData;
	static lThread::LoadSettingFileData* GetSigInstanceInstance(bool deleteFlag = false,const SigInstance* sigInstance= Q_NULLPTR, QWidget* paren = Q_NULLPTR);
};
#endif // H_MY__SINGLETONLOADSETTINGFILEDATA_H__H
