﻿#ifndef H_MY__ALGORITHMTOPITEMWIDGETITEM_H__H
#define	H_MY__ALGORITHMTOPITEMWIDGETITEM_H__H
#include <qboxlayout.h>
#include <qcheckbox.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <QWidget>

class AlgorithmTopItemWidgetItem : public QWidget {
Q_OBJECT;
public:
	QHBoxLayout* hLayout;
	QLabel* expressLabel;
	QCheckBox* checkBox;
	QPushButton* pushButton;
public:
	AlgorithmTopItemWidgetItem(const QString& name
		, const QString& express
		, QWidget* parent = Q_NULLPTR
		, const Qt::WindowFlags& f = Qt::WindowFlags( ));
private:
	void onBtnChicked(bool clicked);
	void onCheckBox(int status);
signals:
	void sigPushChicked(AlgorithmTopItemWidgetItem* item
		, bool status);
	void sigCheckChicked(AlgorithmTopItemWidgetItem* item
		, int status);
};
#endif // H_MY__ALGORITHMTOPITEMWIDGETITEM_H__H
