﻿#pragma once
#include <QWidget>
#include "ui_AlgorithmPage.h"
#include <QIcon>
#include <qtoolbox.h>
#include "AlgorithmItem.h"
#include "AlgorithmTopItemWidget.h"
#include "ATabPageBase.h"

namespace widgetUI {
	class AAlgorithmItem;
	class AlgorithmPage;
}

class widgetUI::AlgorithmPage : public ATabPageBase {
Q_OBJECT;
public:
	void activation(QObject* obj
		, int index
		, EVENTFLAG status) override;
public:
	//void listIndexChanged(int index);
	void appExpreeItem(bool checked);
	void saveData(bool checked);
	//void setDoc(dXml::XmlDataDoc& doc);

	AlgorithmPage(QWidget* parent = Q_NULLPTR);
	~AlgorithmPage( );
private:
	Ui::AlgorithmPage ui;
	QToolBox* toolBox;
	QString currentPath;
	AlgorithmTopItemWidget* globalAlgroithmItem;
	//QHash<QString /*xml 全路径名称*/, AAlgorithmItem*> pageItemMap;
	//QHash<QString /*xml 全路径名称*/, dXml::XmlDataDoc> pageDocMap;
	/*
	 * 混合 doc
	 */
	void blenderFileBuffDoc(dXml::XmlDataDoc doc);
	dXml::XmlDataUnit* unit;
	void initXmlSetting( );
	void initUi( );
	/*void appendItemWidget(AAlgorithmItem* item);*/
	/*
	 * 初始化全局配置选项
	 */
	void initGlobalAlgroithmItem( );
	void initConnect( );
public:
	void selectItem(const QString& itemData);
	/*void appendPage(QString name
		, QString xmlFileFullPath);*/
	/*void appendExpress(const QString& name
		, const QString& express);*/
	QString getName( ) override;
	QIcon getIcon( ) override;
	//void clear( );
	/*public:
		static AlgorithmPage* getInstance( );*/
};
