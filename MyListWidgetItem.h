﻿#ifndef H_MY__MYLISTWIDGETITEM_H__H
#define	H_MY__MYLISTWIDGETITEM_H__H
#include <QListWidgetItem>

class MyListWidgetItem : public QListWidgetItem {
public:
	MyListWidgetItem(QString fullPath, QListWidget* listview = nullptr, int type = Type);
	MyListWidgetItem(QString fullPath, const QString& text, QListWidget* listview = nullptr, int type = Type);
	MyListWidgetItem(QString fullPath, const QIcon& icon, const QString& text, QListWidget* listview = nullptr, int type = Type);
	MyListWidgetItem(QString fullPath, const QListWidgetItem& other);
	QString getFullPath();
private:
	QString fullPath;
};
#endif // H_MY__MYLISTWIDGETITEM_H__H
