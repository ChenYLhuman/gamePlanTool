﻿#include "MyListWidgetItem.h"

MyListWidgetItem::MyListWidgetItem(QString fullPath, QListWidget* listview, int type): QListWidgetItem(listview, type) {
	this->fullPath = fullPath;
}

MyListWidgetItem::MyListWidgetItem(QString fullPath, const QString& text, QListWidget* listview, int type): QListWidgetItem(text, listview, type) {
	this->fullPath = fullPath;
}

MyListWidgetItem::MyListWidgetItem(QString fullPath, const QIcon& icon, const QString& text, QListWidget* listview, int type) : QListWidgetItem(icon, text, listview, type) {
	this->fullPath = fullPath;
}

MyListWidgetItem::MyListWidgetItem(QString fullPath, const QListWidgetItem& other) : QListWidgetItem(other) {
	this->fullPath = fullPath;
}

QString MyListWidgetItem::getFullPath( ) {
	return this->fullPath;
}
