﻿#include "CommunicationPage.h"
#include <QHBoxLayout>
#include <QTextEdit>
#include <QScrollArea>
#include <QListWidgetItem>
#include "qdebug.h"
#include "CommShowMsgItem.h"
#include <QtGlobal>
#include <qmenu.h>
#include <QAction>
#include <QClipboard>
#include <QEvent>
#include <QMouseEvent>
#include <QDir>
#include <QScrollBar>

void widgetUI::CommunicationPage::activation(QObject* obj, int index, EVENTFLAG status) {
	qDebug( ) << getName( );
}

QString widgetUI::CommunicationPage::getName( ) {
	static QString objName = "社交";
	return objName;
}

QIcon widgetUI::CommunicationPage::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

/*
widgetUI::CommunicationPage* widgetUI::CommunicationPage::getInstance( ) {
	static CommunicationPage instance;
	return &instance;
}*/

void widgetUI::CommunicationPage::addMsgItem(CommShowMsgItem* msgItem) {
	ui.showMsgsWidget->layout( )->addWidget(msgItem);
	emit appIndex(msgItem);
}

void widgetUI::CommunicationPage::testMsgItem(int itemCount, int msgCountLen) {
	for( int i = 0;i < itemCount;++i ) {
		int randNumber = qrand( ) % 100;
		CommShowMsgItem::msgType type = ( CommShowMsgItem::msgType )( randNumber % 2 );
		QStringList striList;
		for( int j = 0 - 1;j < msgCountLen;++j )
			striList.append("测试内容..");
		QString imagePath = ( type == CommShowMsgItem::msgType::SENDER
			? ":/image/send.jpg"
			: ":/image/accept.jpg" );
		QString name = QString("测试");
		QString msg = striList.join("");
		addMsgItem(new CommShowMsgItem(type, name, msg, imagePath, this));
	}
}

widgetUI::CommunicationPage::CommunicationPage(QWidget* parent) : ATabPageBase(parent) {
	ui.setupUi(this);
	ui.statuImage->setPixmap(QPixmap(":/image/connect.png").scaled(20, 20, Qt::KeepAspectRatio));
	// CommShowMsgItem(msgType msgBodyType, QString& userName, QString& msg, QString& imagePath, QWidget* parent = Q_NULLPTR)
	testMsgItem(10, 10);
	this->setObjectName(itemName::CommShowMsgItemObjName::getUserImageObjName( ));
	ui.scrollArea->setStyleSheet(QString::fromUtf8("QScrollBar{width:70px;}"));
	/*// 设置过滤器
	installEventFilter(this); /*
	luserMsg->installEventFilter(this);
	luserName->installEventFilter(this);
	userImage->installEventFilter(this);#1#*/
}

/*
bool widgetUI::CommunicationPage::eventFilter(QObject* watched, QEvent* event) {
	static QMenu msgMeun;
	static QMenu userNameMenu;
	static QMenu userImageMenu;
	static bool init = false;
	static int mouseMouseButtonReleaseEvent = 0;
	static QPoint nowPoint;
	static QPoint oldPoint;
	static QString userNameObjName = itemName::CommShowMsgItemObjName::getUserNameObjName( );
	static QString msgObjName = itemName::CommShowMsgItemObjName::getMsgLabelObjName( );
	static QString userImageObjName = itemName::CommShowMsgItemObjName::getUserImageObjName( );
	if( !init ) {
		initMenu(msgMeun, userNameMenu, userImageMenu);
		init = true;
	}
	QEvent::Type eventType = event->type( );
	QPoint pos = QCursor::pos( );
	QWidget* widget = QApplication::widgetAt(pos);
	if( !widget )
		return QWidget::eventFilter(watched, event);
	QString eventName = widget->objectName( );
	if( eventType == QEvent::MouseButtonRelease ) {
		oldPoint = nowPoint;
		if( mouseMouseButtonReleaseEvent == 1 ) {
			qDebug( ) << eventName << "\t释放了左键" << mouseMouseButtonReleaseEvent;
			if( eventName == userNameObjName ) {
				userNameMenu.popup(pos);
				return true;
			}
			if( eventName == msgObjName ) {
				msgMeun.popup(pos);
				return true;
			}
			if( eventName == userImageObjName ) {
				userImageMenu.popup(pos);
				return true;
			}
		} else {
			mouseMouseButtonReleaseEvent = 0;
			QLabel* label = qobject_cast<QLabel*>(watched);
			if( label ) {
				QClipboard* board = QApplication::clipboard( );
				board->setText(label->selectedText( ));
			}
		}
	}
	if( eventType == QEvent::MouseButtonPress ) {
		mouseMouseButtonReleaseEvent = 1;
		nowPoint = QCursor::pos( );
	}
	if( eventType == QEvent::MouseMove ) {
		if( mouseMouseButtonReleaseEvent == 1 )
			mouseMouseButtonReleaseEvent = 2;
		qDebug( ) << eventName << "\t移动\t" << mouseMouseButtonReleaseEvent;
		const QPoint point = nowPoint - oldPoint;
		if( point.y( ) > 0 ) { }
	}
	return QWidget::eventFilter(watched, event);
}
*/

widgetUI::CommunicationPage::~CommunicationPage( ) {}

QScrollBar* widgetUI::CommunicationPage::getVScrollBar( ) {
	return ui.scrollArea->verticalScrollBar( );
}

QScrollBar* widgetUI::CommunicationPage::getHScrollBar( ) {
	return ui.scrollArea->horizontalScrollBar( );
}

QScrollArea* widgetUI::CommunicationPage::getScrollArea( ) {
	return ui.scrollArea;
}

QWidget* widgetUI::CommunicationPage::getMsgBox( ) {
	return ui.showMsgsWidget;
}
