﻿#include "CommShowMsgItemObjName.h"

QString itemName::CommShowMsgItemObjName::getItemObjName( ) {
	static QString itemObjName = "userName";
	return itemObjName;
}

QString itemName::CommShowMsgItemObjName::getMsgBoxObjName( ) {
	static QString itemObjName = "msgBoxObjName";
	return itemObjName;
}

QString itemName::CommShowMsgItemObjName::getUserNameObjName( ) {
	static QString userNameObjName = "userName";
	return userNameObjName;
}

QString itemName::CommShowMsgItemObjName::getUserImageObjName( ) {
	static QString userImageObjName = "userImage";
	return userImageObjName;
}

QString itemName::CommShowMsgItemObjName::getMsgLabelObjName( ) {
	static QString msgLabelObjName = "userMsg";
	return msgLabelObjName;
}
