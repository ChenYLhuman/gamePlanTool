﻿#pragma once
#include <QWidget>
#include "ui_PropertyPage.h"
#include "ATabPageBase.h"
#include "AToolBoxPage.h"
#include "PropertyItem.h"

namespace widgetUI {
	class PropertyPage;
}

class widgetUI::PropertyPage : public ATabPageBase {
Q_OBJECT;
public:
	void activation(QObject* obj
		, int index
		, EVENTFLAG status) override;
public:
	void setFilePath(const QString& filePath);
	void initControl( );
	PropertyPage(QWidget* parent = Q_NULLPTR);
	~PropertyPage( );
	QString currentFilePath;
public:
	// 触发 synInfo
	void emitSynInfo(bool clicked = false);
	
private:
	Ui::PropertyPage ui;
	QVBoxLayout* mainLayout;
	const char* nodeName = "属性";
	dXml::XmlDataUnit* xmlDataUnit;
	PropertyItem* currentEditItem;
	QVector<PropertyItem*>* itemArray;
	QVector<PropertyItem*>* noSaveArray;
	int itemStatus = 0;
	QRegExp removePreZero = QRegExp("^(0+)");
private:
	void test( );
	void connectPropertyItem(PropertyItem* propertyItem);
	void xmlToItem(const QList<dXml::XmlDataUnit*>& list);
	void clear( );
	// 同步信息
	void synInfo(bool clicked);
	void sortItem( );
	static bool sortFun(PropertyItem* left
		, PropertyItem* right);
	// 新建单项信息
	void newInfo(bool clicked);
	void saveToFileSystemBuff( );
	void saveToItem(QString text
		, QString number);
	// 保存到信息列表
	void saveToList(bool clicked);
	// 保存到文件
	void saveToFile(bool clicked);
	// 编辑框发生改变
	void textChanged(const QString& text);
	// 同步翻译信息
	void translating(bool clicked);
private:
	// 控件删除信号
	void removeItem(PropertyItem* item
		, bool clicked);
	// 控件编辑信号
	void editItem(PropertyItem* item
		, bool clicked);
public:
	QString getName( ) override;
	QIcon getIcon( ) override;
	/*public:
		static PropertyPage* getInstance( );*/
};
