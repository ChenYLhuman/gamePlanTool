﻿#include "PropertyItem.h"
#include <QSizePolicy>

PropertyItem::PropertyItem(QWidget* parent, const Qt::WindowFlags& f) : QFrame{ parent
																				,f } {
	static int iStyle = 0;
	delBtn = new QPushButton("删除", this);
	Translation = new QLabel("未翻译", this);
	property = new QLabel("0", this);
	number = new QLabel("0", this);
	status = new QLabel("正常", this);
	property->setAlignment(Qt::AlignCenter);
	number->setAlignment(Qt::AlignCenter);
	status->setAlignment(Qt::AlignCenter);
	editBtn = new QPushButton("编辑", this);
	/*delBtn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	number->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	status->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	Translation->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	property->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	editBtn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);*/
	mainLayout = new QHBoxLayout(this);
	mainLayout->setContentsMargins(0, 0, 0, 0);
	mainLayout->setSpacing(0);
	int stretch = 2;
	mainLayout->addWidget(delBtn, stretch);
	mainLayout->addWidget(number, stretch);
	mainLayout->addWidget(status, stretch);
	mainLayout->addWidget(Translation, stretch);
	mainLayout->addWidget(property, stretch);
	mainLayout->addWidget(editBtn, stretch);
	editBtn->setCheckable(true);
	setObjectName("mainWind");
	if( iStyle % 2 == 1 )
		setStyleSheet("#mainWind{border-style: solid;border-width: 5px;border-bottom-left-radius:20px;     border-color: rgb(103, 197, 186);min-width: 2px;}");
	else
		setStyleSheet("#mainWind{border-style: solid;border-width: 5px;border-bottom-right-radius:20px;     border-color: #df3f4b;min-width: 2px;}");
	++iStyle;
	connect(delBtn, &QPushButton::clicked, this, &PropertyItem::removeBtnSloat);
	connect(editBtn, &QPushButton::clicked, this, &PropertyItem::editBtnSloat);
}

void PropertyItem::removeBtnSloat(bool clicked) {
	emit removeBtnSig(this, clicked);
}

void PropertyItem::editBtnSloat(bool clicked) {
	emit editBtnSig(this, clicked);
}
