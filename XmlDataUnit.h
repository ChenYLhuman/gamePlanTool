﻿#ifndef H_MY__XMLDATAUNIT_H__H
#define	H_MY__XMLDATAUNIT_H__H
#include <QFileInfoList>
#include "dXml.h"
#include <qlist.h>
#include <qxmlstream.h>

class dXml::XmlDataUnit {
protected:
	// 父节点
	XmlDataUnit* paren = Q_NULLPTR;
	// 子对象
	QList<XmlDataUnit*>* children = Q_NULLPTR;
	// 属性
	QHash<QString, QString>* attrs = Q_NULLPTR;
	// 节点名称
	QString name;
	// 注释
	//QStringList comments;
	// 节点文本
	QString text;
	XmlDataUnit* clonChildren(XmlDataUnit* xmlObj
		, XmlDataUnit* newObjParen);
public:
	XmlDataUnit( );
	XmlDataUnit(const XmlDataUnit& xmlObj);
	XmlDataUnit& operator=(const XmlDataUnit& xmlObj);
	/*// 设置注释
	void setComment(const QStringList& com);
	void setComment(const QString& com);
	void appendComment(const QString& com);
	void appendComment(const QStringList& com);
	// 获取注释
	const QString getComment(const QString& jion);
	const QString getComment(const char*& jion);
	const QString getComment(const char& jion);*/
	// 创建节点，并且赋予名称
	XmlDataUnit(const QString& Nodename);
	virtual void sort(std::function<bool(XmlDataUnit* left
		, XmlDataUnit* right)> sorFun);
	/*// 设置注释
	void setComment(QString& comment);
	// 追加注释
	void appendComment(QString& comment);*/
	// 清除内容
	virtual void clear( );
	// 基于当前数据结构获取 xpath 信息
	const QHash<QString, QList<dXml::XmlDataUnit*>> getXpath( );
	// 设置父节点
	virtual void setParen(XmlDataUnit* paren
		, int index = -1);
	// 获取父节点
	virtual XmlDataUnit* getParen( ) const;
	// 返回第一个匹配的节点
	virtual dXml::XmlDataUnit* findChildren(const QString& name);
	// 根据名称获取子节点
	virtual QList<XmlDataUnit*> findChildrens(const QString& name);
	// 获取所有子节点
	virtual QList<XmlDataUnit*>* getChildrens( ) const;
	// 返回当前节点名称
	virtual QString nodeName( ) const;
	// 设置当前节点名称
	virtual void setName(const QString& name);
	// 获取属性
	virtual QHash<QString, QString>* getAttrs( ) const;
	// 获取文本
	virtual QString& getText( );
	// 添加一个属性
	virtual void appendAttr(const QString& attr
		, const QString& value);
	// 添加多个属性
	virtual void appendAttrs(QHash<QString, QString>& attrs);
	virtual void appendAttrs(QList<QString>& attrs
		, QList<QString>& values);
	// 设置文本内容，clearChilder 是否清楚子对象列表
	virtual bool setText(const QString& text
		, bool clearChilder = false);
	// 添加一个子节点
	virtual void appendChildren(XmlDataUnit* children
		, int index = -1);
	// 添加多个子节点
	virtual void appendChildrens(QList<XmlDataUnit*>& childrens
		, int index = -1);
	// bool writeXmlDataToFile(QXmlStreamWriter& writer, QXmlStreamAttributes& attributes, XmlDataUnit* unit);
	// 保存到文件
	virtual bool save(QFile& file
		, const char* processingInstructionData = Q_NULLPTR
		, const char* processingInstructionTarget = Q_NULLPTR
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);
	virtual bool save(QFile& file
		, QDomDocument& qDomDocument
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);
	virtual bool save(const QString& file
		, QDomDocument& qDomDocument
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);
	virtual bool save(const QString& file
		, QDomDocument* qDomDocument = Q_NULLPTR
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);
	virtual bool save(const char* file
		, QDomDocument* qDomDocument = Q_NULLPTR
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);


	// 获取子节点个数
	virtual int getChildrensSize( ) const;
	// 释放节点
	virtual ~XmlDataUnit( );
};
#endif // H_MY__XMLDATAUNIT_H__H
