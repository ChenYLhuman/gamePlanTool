﻿#ifndef H_MY__APTR_H__H
#define	H_MY__APTR_H__H
#include <qcompilerdetection.h>

template<class ptrName>
class aPtr {
public:
	ptrName ptr;

	aPtr(ptrName ptr = Q_NULLPTR) : ptr{ ptr } {}
};
#endif // H_MY__APTR_H__H
