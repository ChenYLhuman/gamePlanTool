﻿#ifndef H_MY__PROPERTYITEM_H__H
#define	H_MY__PROPERTYITEM_H__H
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QHBoxLayout>

class PropertyItem : public QFrame {
Q_OBJECT;
private:
	QHBoxLayout* mainLayout;
public:
	// 删除属性
	QPushButton* delBtn;
	// 编号
	QLabel* number;
	// 状态
	QLabel* status;
	// 翻译
	QLabel* Translation;
	// 属性
	QLabel* property;
	// 编辑当前
	QPushButton* editBtn;
	PropertyItem(QWidget* parent = Q_NULLPTR, const Qt::WindowFlags& f = Qt::WindowFlags( ));
private:
	void removeBtnSloat(bool clicked);
	void editBtnSloat(bool clicked);
signals:
	void removeBtnSig(PropertyItem* item, bool clicked);
	void editBtnSig(PropertyItem* item, bool clicked);
};
#endif // H_MY__PROPERTYITEM_H__H
