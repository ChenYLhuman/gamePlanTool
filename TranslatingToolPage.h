﻿#ifndef H_MY__TRANSLATINGTOOLPAGE_H__H
#define	H_MY__TRANSLATINGTOOLPAGE_H__H
#include <QHBoxLayout>
#include "AToolBoxPage.h"
#include <qicon.h>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QTextCursor>
#include "XmlDataDoc.h"

namespace widgetUI {
	class TranslatingToolPage;
	class TranslatingItem;
	class TranslatingLineEdit;
}

class widgetUI::TranslatingToolPage : public widgetUI::AToolBoxPage {
Q_OBJECT;
private:
	QString objPageName = "翻译";
	QIcon objPageIcon;
	dXml::XmlDataUnit* xmlUnit = Q_NULLPTR;
	QVBoxLayout* mainLayout = Q_NULLPTR;
	QSpacerItem* spacerItem = Q_NULLPTR;
public:
	void clear( ) override;
private:
	QList<TranslatingItem*>* translatingSetting = Q_NULLPTR;
	QHash<int, dXml::XmlDataUnit*>* tMapXmlNodeSetting = Q_NULLPTR;
	QHash<int, QString>* tMapXmlTranslating = Q_NULLPTR;
private:
	// 排序函数
	static bool sortOrder(TranslatingItem* barAmount1, TranslatingItem* barAmount2);
	static bool sortReversingOrder(TranslatingItem* barAmount1, TranslatingItem* barAmount2);
	void appItems(QList<dXml::XmlDataUnit*>* nodes, dXml::XmlDataUnit* paren);
	void translatingChanged(QString& text);
private: // 事件
	void saveClicked(TranslatingLineEdit* editBar, QPushButton* chickedObj, bool chicked);
	void lineEditChaned(const TranslatingItem* item, const QString& text);
	void LineEditFocusInEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
	void LineEditFocusOutEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
public:
	const QHash<int, QString> getTranslating( );
	dXml::XmlDataDoc getItemsToDoc( );
	// 对控件进行排序 order = true 编号顺序，order = false 编号倒序
	void sortWidget(bool order = false) override;
	TranslatingToolPage(QWidget* paren = Q_NULLPTR);
	virtual ~TranslatingToolPage( );
	void activation(QObject* obj, int index) override;
	QString getName( ) override;
	void setName(QString& name) override;
	QIcon getIcon( ) override;
	void setIcon(QIcon& icon) override;
	void setTitle(QString& str) override;
	dXml::XmlDataUnit getXmlUnityToCloneObj( ) override;
	bool synchXmlUnit( ) override;
	void objConnectSig(widgetUI::TranslatingItem* item);
	QList<QWidget*> setXmlUnityDom(dXml::XmlDataUnit* mode);
	QList<QWidget*> setXmlUnityDoms(QList<dXml::XmlDataUnit*>& modes);
	QWidget* addItem(QIcon icon, int nodeCode, const QString& Data, QHash<QString, QString>* attrMap = Q_NULLPTR);
	QWidget* addItem(int nodeCode, const QString& Data, QHash<QString, QString>* attrMap = Q_NULLPTR);
};

class widgetUI::TranslatingItem : public QWidget {
Q_OBJECT;
public:
	QPushButton* btn;
	TranslatingLineEdit* edit;
	QHBoxLayout* mainLayout;
	void inintConnect( );
	TranslatingItem(QWidget* parent = Q_NULLPTR, const Qt::WindowFlags& f = Qt::WindowFlags( ));
	TranslatingItem(QPushButton* btn, TranslatingLineEdit* edit = Q_NULLPTR, QWidget* parent = Q_NULLPTR, const Qt::WindowFlags& f = Qt::WindowFlags( ));
	TranslatingItem(const QString& btnName, const QString& content, QWidget* parent = Q_NULLPTR);
private:
	void onBtnClicked(bool clicked);
	void lineEditChaned(const QString& text);
signals:
	void btnOnClicked(TranslatingLineEdit* editBar, QPushButton* chickedObj, bool chicked);
	void updataItem(TranslatingLineEdit* editBar, QPushButton* chickedObj, bool chicked);
	void translatingLineEditChaned(const TranslatingItem* item, const QString& text);
	void LineEditFocusInEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
	void LineEditFocusOutEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
};

class widgetUI::TranslatingLineEdit : public QLineEdit {
Q_OBJECT;
public:
	TranslatingLineEdit(QWidget* parent) : QLineEdit{ parent } { }

	TranslatingLineEdit(const QString& chars, QWidget* parent) : QLineEdit{ chars
																			,parent } { }

protected:
	void focusInEvent(QFocusEvent*) override;
	void focusOutEvent(QFocusEvent*) override;
signals:
	void LineEditFocusInEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
	void LineEditFocusOutEvent(TranslatingLineEdit* EventObj, QFocusEvent* event);
};
#endif // H_MY__TRANSLATINGTOOLPAGE_H__H
