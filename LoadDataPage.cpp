﻿#include "LoadDataPage.h"
#include <QTextEdit>
#include <QSharedMemory>
#include <qapplication.h>
#include <qstyle.h>
#include <QIcon>
#include <qdebug.h>
#include "MyApplication.h"
#include <fstream>
#include <QDir>
#include <QDateTime>
#include <QHeaderView>
#include <QScreen>
#include <QMouseEvent>
#include <QScrollBar>
#include <QEvent>
#include <QListWidget>
#include <QProgressBar>
#include "TreeWidgetEventFilter.h"
using namespace widgetUI;

void LoadDataPage::activation(QObject* obj
	, int index
	, EVENTFLAG status) {
	qDebug( ) << getName( );
}

void LoadDataPage::initControl( ) {
	ui.filePath->setText(QDir::currentPath( ));
	QPushButton* button = ui.updateDir;
	QList<QPushButton*> buttons = findChildren<QPushButton*>( );
	for( auto& btn : buttons ) {
		connect(btn, &QPushButton::clicked, this, &LoadDataPage::sendSignal);
	}
}

LoadDataPage::LoadFlag LoadDataPage::setBtnSignals(LoadDataPage::LoadFlag flag) {
	static LoadDataPage::LoadFlag status = LoadDataPage::LoadFlag::CANCEL;
	if( flag != LoadDataPage::LoadFlag::GETFLAG )
		status = flag;
	return status;
}

LoadDataPage::LoadFlag LoadDataPage::setBtnSignals(int flag) {
	return setBtnSignals(( LoadFlag )flag);
}

LoadDataPage::LoadFlag LoadDataPage::addBtnSignals(LoadFlag flag) {
	int newFlag = flag | setBtnSignals( );
	return setBtnSignals(( LoadFlag )newFlag);
}

LoadDataPage::LoadFlag LoadDataPage::subBtnSignals(LoadFlag flag) {
	int newFlag = flag ^ setBtnSignals( );
	return setBtnSignals(( LoadFlag )newFlag);
}

LoadDataPage::LoadFlag LoadDataPage::clearBtnSignals(LoadFlag flag) {
	return setBtnSignals(LoadFlag::CANCEL);
}

void LoadDataPage::initTreeView( ) {
	// ui.filePath->setReadOnly(true);
	QTreeWidget* widget = ui.fileTree;
	QHeaderView* headView = widget->header( );
	headView->setSectionsMovable(false);
	headView->setFrameStyle(QFrame::Plain | QFrame::WinPanel);
	headView->setObjectName(MyApplication::objName::loadPageTreeWdigetHeadViewObjName);
	headView->setSectionResizeMode(QHeaderView::ResizeToContents);
	// widget->setStyleSheet(QString::fromUtf8("QScrollBar{width:1px;}"));
	ui.vScrollBar->setStyleSheet(QString::fromUtf8("QScrollBar{width:70px;}"));
	ui.hScrollBar->setStyleSheet(QString::fromUtf8("QScrollBar{width:70px;}"));
	int scrollBarWdith = headView->verticalScrollBar( )->size( ).width( );
	headView->resizeSection(headView->count( ) - 1, scrollBarWdith);
	widget->setSortingEnabled(true);
	widget->header( )->setSortIndicator(0, Qt::AscendingOrder);
	widget->header( )->setSortIndicatorShown(true);
	// new TreeWidgetEventFilter(widget);
	connect(widget, &QTreeWidget::currentItemChanged, this, &LoadDataPage::currentItemChanged);
	//connect(widget, &QTreeWidget::itemExpanded, this, &LoadDataPage::treeItemExpanded);
	QScrollBar* widgetVscrollBar = widget->verticalScrollBar( );
	QScrollBar* widgetHscrollBar = widget->horizontalScrollBar( );
	ui.vScrollBar->setRange(0, 0);
	//	ui.hScrollBar->setRange(0, 0);
	connect(widgetVscrollBar, &QScrollBar::rangeChanged, ui.vScrollBar, &QScrollBar::setRange);
	connect(widgetVscrollBar, &QScrollBar::actionTriggered, this, &LoadDataPage::chenageVValue);
	connect(ui.vScrollBar, &QScrollBar::valueChanged, widgetVscrollBar, &QScrollBar::setValue);
	connect(widgetHscrollBar, &QScrollBar::rangeChanged, ui.hScrollBar, &QScrollBar::setRange);
	connect(ui.hScrollBar, &QScrollBar::valueChanged, widgetHscrollBar, &QScrollBar::setValue);
	/*widget->setVerticalScrollBar(ui.vScrollBar);
	widget->setHorizontalScrollBar(ui.hScrollBar);*/
}

void LoadDataPage::chenageVValue(int position) {
	ui.vScrollBar->setValue(ui.fileTree->verticalScrollBar( )->value( ));
}

LoadDataPage::LoadDataPage(QWidget* parent) : ATabPageBase(parent) {
	ui.setupUi(this);
	initControl( );
	initTreeView( );
	setObjectName(getName( ));
	grabGesture(Qt::PanGesture);
	Progress(ui.progressBar, true);
	ui.progressBar->setStyleSheet("QProgressBar {	border: 2px solid grey;   	border-radius: 5px;   	background-color: #FFFFFF;}QProgressBar::chunk {   	background-color: #05B8CC;   }QProgressBar {   	border: 2px solid grey;   	border-radius: 5px;   	text-align: center;font: 81 10pt ;}");
}

LoadDataPage::~LoadDataPage( ) {}

QLineEdit* LoadDataPage::getFilePath( ) {
	return ui.filePath;
}

QTreeWidget* LoadDataPage::getWidgetTreeWidget( ) {
	return ui.fileTree;
}

/*
LoadDataPage* LoadDataPage::getInstance( ) {
	static LoadDataPage instance;
	return &instance;
}
*/

QString LoadDataPage::getName( ) {
	static QString objName = "载入...";
	return objName;
}

QIcon LoadDataPage::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

QProgressBar* LoadDataPage::Progress(QProgressBar* progressBar
	, bool forceSet) {
	static QProgressBar* progressPtr = Q_NULLPTR;
	if( forceSet || progressBar )
		progressPtr = progressBar;
	return progressPtr;
}

void LoadDataPage::sendSignal(bool status) {
	auto&& btn_signals = setBtnSignals( );
	if( btn_signals == LOADDATAPAGESIGNALSFLAG::CANCEL ) {
		return;
	}
	auto control_obj = qobject_cast<QPushButton*>(sender( ));
	QString name = control_obj->objectName( );
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::GLOBALBTNSIGNALS ) {
		emit onClick(this, control_obj, status, ui.fileTree, ui.filePath);
	}
	if( name.isEmpty( ) )
		return;
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::CURRENTDIRBTNSIGNALS && name == ui.resetPathDir->objectName( ) ) emit resetPathDirBtn(this, control_obj, status, ui.fileTree, ui.filePath);
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::UPDATEBTNSIGNALS && name == ui.updateDir->objectName( ) ) emit updateBtn(this, control_obj, status, ui.fileTree, ui.filePath);
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::UPDIRBTNSIGNALS && name == ui.upDir->objectName( ) ) emit upDirBtn(this, control_obj, status, ui.fileTree, ui.filePath);
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::MENUBTNSIGNALS && name == ui.popMume->objectName( ) ) emit menuBtn(this, control_obj, status, ui.fileTree, ui.filePath);
	if( btn_signals & LOADDATAPAGESIGNALSFLAG::STOPUPDATEBTNSIGNALS && name == ui.stopUpdate->objectName( ) ) emit stopUpdateBtn(this, control_obj, status, ui.fileTree, ui.filePath);
}

/*
void LoadDataPage::treeItemExpanded(QTreeWidgetItem* item) {
	qDebug( ) << __func__;
	qDebug( ) << ui.fileTree->verticalScrollBar( )->value( );
}
*/
