﻿#ifndef H_MY__TREEWIDGETEVENTFILTER_H__H
#define	H_MY__TREEWIDGETEVENTFILTER_H__H
#include <QObject>
#include <QScrollBar>
class QTreeWidget;

class TreeWidgetEventFilter : public QObject{
	Q_OBJECT;
public:
	bool eventFilter(QObject* watched, QEvent* event) override;

	TreeWidgetEventFilter(QTreeWidget* parent = Q_NULLPTR);
	virtual ~TreeWidgetEventFilter();
private:
	QScrollBar *horScrollBar;
	QScrollBar *verScrollBar;
};
#endif // H_MY__TREEWIDGETEVENTFILTER_H__H
