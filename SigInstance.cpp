﻿#include "SigInstance.h"
#include "PageWidgetS.h"
#include <QModelIndex>
#include <QStringList>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>
#include <QFile>
#include <QTreeWidgetItem>
#include <QAbstractItemModel>
#include <qlist.h>
#include "FileBuffSystem.h"
#include "LoadSettingFileData.h"
#include "LoadDiscDirInfoThread.h"
#include "MenuPage.h"
#include "singletonPageWidgetS.h"
#include "singletonMsgAppTxt.h"
#include "singletonLoadSettingFileData.h"
#include "singletonLoadThread.h"
#include "MyTreeWidgetItemData.h"
#include "MyListWidgetItem.h"

QMutex* SigInstance::m_mutex( ) {
	static QMutex mutex;
	return &mutex;
}

void SigInstance::tabWidgetCurrentChanged(int index
	, QWidget* sendSignalObj
	, QTabWidget* tabBody
	, widgetUI::ATabPageBase* page) {
	QString msg = QString("%1\t").arg(index) + __func__;
	msg::IMsgAppTxt* msgSendObj = appInstances::MsgAppTxt::instance( );
	msgSendObj->appendMsg(msg, "逗比\n\t===========\n");
	page->activation(page, index, widgetUI::ATabPageBase::Changed);
}

void SigInstance::tabWidgetTabBarClicked(int index
	, QWidget* sendSignalObj
	, QTabWidget* tabBody
	, widgetUI::ATabPageBase* page) {
	QString msg = QString("%1\t").arg(index) + __func__;
	msg::IMsgAppTxt* msgSendObj = appInstances::MsgAppTxt::instance( );
	msgSendObj->appendMsg(msg, "逗比\n\t===========\n");
	page->activation(page, index, widgetUI::ATabPageBase::Changed);
}

void SigInstance::tabWidgetTabBarDoubleClicked(int index
	, QWidget* sendSignalObj
	, QTabWidget* tabBody
	, widgetUI::ATabPageBase* page) {
	QString msg = QString("%1\t").arg(index) + __func__;
	msg::IMsgAppTxt* msgSendObj = appInstances::MsgAppTxt::instance( );
	msgSendObj->appendMsg(msg, "逗比\n\t===========\n");
	page->activation(page, index, widgetUI::ATabPageBase::Changed);
}

void SigInstance::onClick(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {}

void SigInstance::updateBtn(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {
	auto foreachPath = filePath->text( );
	QString timeFormat = "yyyy:dd:MM:hh:ss:ss.z";
	auto load = appInstances::LoadDiscDirInfoThread::GetSigInstanceInstance( );
	load->setTask(treeWidget, &foreachPath, &timeFormat);
	load->startTask( );
}

void SigInstance::menuBtn(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {
	static QMenu menu;
	static bool isInit = false;
	if( !isInit ) {
		QAction* action = menu.addAction("你好");
		action->setEnabled(true);
		connect(action, &QAction::triggered, [=](bool checked) {
			qDebug( ) << "点击了 Action";
		});
	}
	menu.move(QCursor::pos( ));
	menu.show( );
}

void SigInstance::m_upDirBtn(QLineEdit* filePath) {
	QDir filesystedir(filePath->text( ));
	if( filesystedir.cdUp( ) )
		filePath->setText(filesystedir.absolutePath( ));
}

void SigInstance::upDirBtn(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {
	m_upDirBtn(filePath);
}

void SigInstance::resetPathDirBtn(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {
	filePath->setText(QDir::currentPath( ));
}

void SigInstance::stopUpdateBtn(QWidget* mainWidget
	, QPushButton* controlObj
	, bool status
	, QTreeWidget* treeWidget
	, QLineEdit* filePath) {
	auto load = appInstances::LoadDiscDirInfoThread::GetSigInstanceInstance( );
	load->stopTask( );
}

void SigInstance::currentItemChanged(QTreeWidgetItem* current
	, QTreeWidgetItem* previous) {
	MyTreeWidgetItemData* currentData = dynamic_cast<MyTreeWidgetItemData*>( current );
	if( !currentData )
		return;
	QString fileType = currentData->data(1, 0).toString( );
	QProgressBar* progress_bar = widgetUI::LoadDataPage::Progress( );
	if( fileType == "文件" ) {
		progress_bar->setFormat("%p%-error !文件无法被获取");
		progress_bar->setStyleSheet("QProgressBar {	border: 2px solid grey;   	border-radius: 5px;   	background-color: #FFFFFF;}QProgressBar::chunk {   	background-color: #05B8CC;   }QProgressBar {   	border: 2px solid grey;   	border-radius: 5px;   	text-align: center;	color: rgb(130, 10, 18);font: 81 10pt ;}");
		return;
	} else {
		progress_bar->setFormat("%p%---左侧列表显示内容");
		progress_bar->setStyleSheet("QProgressBar {	border: 2px solid grey;   	border-radius: 5px;   	background-color: #FFFFFF;}QProgressBar::chunk {   	background-color: #05B8CC;   }QProgressBar {   	border: 2px solid grey;   	border-radius: 5px;   	text-align: center;color: 	color: rgb(198, 99, 33);font: 81 10pt ;}");
	}
	int count = currentData->childCount( );
	widgetUI::AlgorithmPage* algorithmPage = mainWindow->getAlgorithmPage( );
	if( count ) {
		//algorithmPage->clear( );
		mainWindow->clearList( );
		progress_bar->setRange(0, count);
		progress_bar->setValue(0);
		for( int i = 0;i < count;++i ) {
			QTreeWidgetItem* child = current->child(i);
			QString fileBseName = child->data(0, 0).toString( );
			fileType = child->data(1, 0).toString( );
			progress_bar->setValue(progress_bar->value( ) + 1);
			const QString fullFilePath = currentData->fullPath + "/" + fileBseName;
			if( fileType == "文件" ) {
				progress_bar->setFormat("%p%---跳过文件\t" + fileBseName);
				// todo : 添加xml
				QDir fileDir(currentData->fullPath);
				fileDir.setFilter(QDir::Files);
				QStringList listFilters;
				listFilters << "*.xml";
				fileDir.setNameFilters(listFilters);
				QList<QFileInfo> infoList = fileDir.entryInfoList( );
				widgetUI::MenuPage* menuPage = widgetUI::MainWindow::getMenuPage( );
				menuPage->clearListBox( );
				for( int index = 0;index < infoList.length( );++index ) {
					const QFileInfo info = infoList.at(index);
					QString fileBaseName = info.fileName( );
					//	const QString fileFullPath = fullFilePath + "/" + fileBaseName;
					menuPage->addListItem(fullFilePath, fileBaseName);
				}
				appInstances::FileBuffSystem::fileInstance.addFilePath(fullFilePath);
				continue;
			}
			progress_bar->setFormat("%p%---添加文件\t" + fileBseName);
			mainWindow->additemToList(new MyListWidgetItem(fullFilePath, fileBseName));
			//algorithmPage->appendPage(fileBseName, fullFilePath);
			appInstances::FileBuffSystem::fileInstance.addFileDir(fullFilePath);
		}
	}
	appInstances::FileBuffSystem::fileInstance.setCurrentDir(currentData->fullPath);
	//QFileInfo info(currentData->fullPath);
	//auto fileFull = currentData->fullPath + "/" + info.baseName( ) + ".xmlAlgo";
	//dXml::XmlDataDoc xmlDataDoc;
	//dXml::readXmlFile(&xmlDataDoc, fileFull);
	//appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileFull, xmlDataDoc);
	//algorithmPage->setDoc(xmlDataDoc);
}

void SigInstance::overLoadDataThread(lThread::LoadDiscDirInfoThread* threadObj
	, QTreeWidget* treeWidget
	, QString rootFilePath
	, QString timeFromat
	, QList<MyTreeWidgetItemData*>* treeList
	, int runCode) {
	if( treeWidget )
		treeWidget->clear( );
	if( runCode != 1 ) {
		while( !treeList->isEmpty( ) ) {
			delete treeList->last( );
			treeList->removeLast( );
		}
	} else if( runCode == 1 ) {
		QFileInfo info(rootFilePath);
		QStringList list = QStringList( ) << rootFilePath << ( info.isFile( ) ? "文件" : "文件夹" ) << info.created( ).toString(timeFromat);
		MyTreeWidgetItemData* rootItem = new MyTreeWidgetItemData(treeWidget, list, rootFilePath);
		while( !treeList->isEmpty( ) ) {
			rootItem->addChild(treeList->last( ));
			treeList->removeLast( );
		}
	}
	delete treeList;
	if( !treeWidget || !threadObj )
		return;
	// todo : 是否决定展开列表
	// treeWidget->expandAll( );
}

void SigInstance::startLoadDataThread(lThread::LoadDiscDirInfoThread* threadObj) {
	QProgressBar* progress_bar = widgetUI::LoadDataPage::Progress( );
	progress_bar->setStyleSheet("QProgressBar {	border: 2px solid grey;   	border-radius: 5px;   	background-color: #FFFFFF;}QProgressBar::chunk {   	background-color: #05B8CC;   }QProgressBar {   	border: 2px solid grey;   	border-radius: 5px;   	text-align: center;color: 	color: rgb(198, 99, 33);font: 81 10pt ;}");
}

void SigInstance::execute(int taskNumber
	, int taskIndex
	, const QString& pathFull
	, int status) {
	QProgressBar* progress_bar = widgetUI::LoadDataPage::Progress( );
	if( !progress_bar )
		return;
	progress_bar->setMaximum(taskNumber);
	progress_bar->setValue(taskIndex);
	int length = pathFull.length( );
	QString last = "-%p%";
	if( status > 0xf0 )
		last.append("-被终止--");
	if( length < 15 )
		progress_bar->setFormat(pathFull + last);
	else {
		QDir dir(pathFull);
		QString showMsg;
		for( bool i = true;true; ) {
			bool cd_up = dir.cdUp( );
			if( i ) {
				int dirlength = dir.absolutePath( ).length( );
				showMsg.append("......");
				showMsg.append(pathFull.mid(dirlength, length));
				i = false;
			}
			if( !cd_up )
				break;
		}
		showMsg.insert(0, dir.absolutePath( ));
		progress_bar->setFormat(showMsg + last);
	}
}

void SigInstance::listClick(MyListWidgetItem* item) {
	auto fullPath = item->getFullPath( );
	QString Name = item->text( );
	QString fileName = fullPath + '/' + Name;
	fileName.append(".xml");
	dXml::XmlDataDoc doc;
	QString piTaeget;
	QString piData;
	dXml::readXmlFile(&doc, piTaeget, piData, fileName);
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileName, doc);
	// 触发角色属性面板
	widgetUI::PropertyPage* propertyPage = mainWindow->getPropertyPage( );
	propertyPage->setFilePath(fileName);
	propertyPage->emitSynInfo(false);
	// 触发算法面板
	/*widgetUI::AlgorithmPage* algorithmPage = mainWindow->getAlgorithmPage(  );
	algorithmPage->appendPage(Name, fileName);*/
}

void SigInstance::creatorPerson(widgetUI::MainWindow* eventObj
	, QPushButton* btn
	, bool Clicked) {
	QString personNmae = eventObj->person->text( );
	if( personNmae.isEmpty( ) )
		return;
	const QString currentDir = appInstances::FileBuffSystem::fileInstance.getCurrentDir( );
	if( currentDir.isEmpty( ) )
		return;
	const QString fullFilePath = currentDir + "/" + personNmae;
	QDir dir(fullFilePath);
	bool existe = dir.exists( );
	if( existe )
		return;
	dir.mkpath(fullFilePath);
	widgetUI::LoadDataPage* loadDataPage = eventObj->getLoadDataPage( );
	auto foreachPath = loadDataPage->getFilePath( )->text( );
	QString timeFormat = "yyyy:dd:MM:hh:ss:ss.z";
	auto load = appInstances::LoadDiscDirInfoThread::GetSigInstanceInstance( );
	load->setTask(loadDataPage->getWidgetTreeWidget( ), &foreachPath, &timeFormat);
	load->startTask( );
	mainWindow->additemToList(new MyListWidgetItem(fullFilePath, personNmae));
}

SigInstance::~SigInstance( ) {}
