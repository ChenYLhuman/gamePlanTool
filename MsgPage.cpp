﻿#include "MsgPage.h"
#include <QDir>
#include <qapplication.h>
#include <qstyle.h>
#include <QIcon>
#include <qdebug.h>
#include <QArrayData>
using namespace widgetUI;
using namespace msg;

QString MsgPage::JoinChars(QString chars) {
	static QString appChars = "\n";
	QString oldChars = appChars;
	if( chars.isEmpty( ) )
		return appChars;
	appChars = chars;
	return oldChars;
}

QString MsgPage::getName( ) {
	static QString objName = "消息";
	return objName;
}

QIcon MsgPage::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

/*
MsgPage* MsgPage::getInstance( ) {
	static MsgPage instance;
	return &instance;
}
*/

bool MsgPage::objIsValid( ) {
	return objValidStatus( );
}

IMsgStream* MsgPage::getMsgStreamInstance( ) {
	return this;
}

IMsgStream* MsgPage::getMsgAppTxtInstance( ) {
	return this;
}

void MsgPage::activation(QObject* obj, int index, EVENTFLAG status) {
	qDebug( ) << getName( );
}

MsgPage::MsgPage(QWidget* parent) : ATabPageBase(parent) {
	ui.setupUi(this);
	bool visible = true;
	ui.msg->setReadOnly(visible);
	setObjectName(getName( ));
	objValidStatus(&visible);
}

MsgPage::~MsgPage( ) {
}

IMsgStream& MsgPage::operator<<(const QString& msgTest) {
	const QString text = ui.msg->toPlainText( ) + msgTest + JoinChars( );
	ui.msg->setText(text);
	return *this;
}

IMsgStream& MsgPage::operator<<(const QList<QString>& msgTest) {
	QString text = ui.msg->toPlainText( );
	for( auto& str : msgTest )
		text += str + JoinChars( );
	ui.msg->setText(text);
	return *this;
}

IMsgStream& MsgPage::operator<<(const QStringList& msgTest) {
	QString text = ui.msg->toPlainText( );
	ui.msg->setText(text + msgTest.join(JoinChars( )));
	return *this;
}

bool MsgPage::objValidStatus(bool* status) {
	static bool isValid = false;
	if( status == Q_NULLPTR )
		return isValid;
	isValid = *status;
	return isValid;
}

void MsgPage::appendMsg(const QString& msg, const QString& appChars) {
	QString oldChars = JoinChars(appChars);
	*this << msg;
	JoinChars(oldChars);
}

void MsgPage::appendMsg(const QStringList& msg, const QString& appChars) {
	QString oldChars = JoinChars(appChars);
	*this << msg;
	JoinChars(oldChars);
}

void MsgPage::appendMsg(const QList<QString>& msg, const QString& appChars) {
	QString oldChars = JoinChars(appChars);
	*this << msg;
	JoinChars(oldChars);
}
