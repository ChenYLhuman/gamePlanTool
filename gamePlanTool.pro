QT       += core gui location
QT       += positioning  uitools
android:{
    QT += androidextras
}



QT += network networkauth
QT += opengl openglextensions
QT += multimedia multimediawidgets
QT += xml xmlpatterns
QT += sql networkauth
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ATabPageBase.cpp \
    AToolBoxPage.cpp \
    AlgorithmPage.cpp \
    CommShowMsgItem.cpp \
    CommShowMsgItemObjName.cpp \
    CommunicationPage.cpp \
    DxmlSttringMap.cpp \
    EOperateMouseEvent.cpp \
    FileBuffSystem.cpp \
    LanguageToolPage.cpp \
    LoadDataPage.cpp \
    LoadDiscDirInfoThread.cpp \
    LoadSettingFileData.cpp \
    LoadThreadSettingToolBoxPage.cpp \
    MenuPage.cpp \
    MsgPage.cpp \
    MyApplication.cpp \
    MyListWidgetItem.cpp \
    MyTreeWidgetItemData.cpp \
    PageWidgetS.cpp \
    PropertyItem.cpp \
    PropertyPage.cpp \
    SappInstance.cpp \
    SigInstance.cpp \
    TranslatingToolPage.cpp \
    TreeWidgetEventFilter.cpp \
    XmlDataDoc.cpp \
    XmlDataUnit.cpp \
    aPtr.cpp \
    dXml.cpp \
    main.cpp \
    mainwindow.cpp \
    menus.cpp \
    singletonLoadSettingFileData.cpp \
    singletonLoadThread.cpp \
    singletonMenus.cpp \
    singletonMsgAppTxt.cpp \
    singletonMsgStream.cpp \
    singletonPageWidgetS.cpp

HEADERS += \
    ATabPageBase.h \
    AToolBoxPage.h \
    AlgorithmPage.h \
    CommShowMsgItem.h \
    CommShowMsgItemObjName.h \
    CommunicationPage.h \
    DxmlSttringMap.h \
    EOperateMouseEvent.h \
    FileBuffSystem.h \
    IMsgStream.h \
    ISigAbstract.h \
    LanguageToolPage.h \
    LoadDataPage.h \
    LoadDiscDirInfoThread.h \
    LoadSettingFileData.h \
    LoadThreadSettingToolBoxPage.h \
    MenuPage.h \
    MsgPage.h \
    MyApplication.h \
    MyListWidgetItem.h \
    MyTreeWidgetItemData.h \
    PageWidgetS.h \
    PropertyItem.h \
    PropertyPage.h \
    SappInstance.h \
    SigInstance.h \
    TranslatingToolPage.h \
    TreeWidgetEventFilter.h \
    XmlDataDoc.h \
    XmlDataUnit.h \
    aPtr.h \
    dXml.h \
    mainwindow.h \
    menus.h \
    singletonLoadSettingFileData.h \
    singletonLoadThread.h \
    singletonMenus.h \
    singletonMsgAppTxt.h \
    singletonMsgStream.h \
    singletonPageWidgetS.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

FORMS += \
    AlgorithmPage.ui \
    CommunicationPage.ui \
    LoadDataPage.ui \
    MenuPage.ui \
    MsgPage.ui \
    PropertyPage.ui

RESOURCES += \
    GamePlanTool.qrc \
    GamePlanTool.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    gamePlanTool.pro.user \
    gamePlanTool.sln \
    gamePlanTool.vcxproj \
    gamePlanTool.vcxproj.filters \
    gamePlanTool.vcxproj.user

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
