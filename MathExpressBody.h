﻿#ifndef H_MY__MATHEXPRESSBODY_H__H
#define	H_MY__MATHEXPRESSBODY_H__H
#include "dXml.h"

// 数学表达式数据结构
class dXml::MathExpress::MathExpressBody {
	friend class dXml::MathExpress::MathManage;
	friend class dXml::MathExpress::MathExpressResolve;
	// 表达式左下标
	int leftIndex;
	// 表达式右下标
	int rightIndex;
	// 表达式首节序
	int beforMake;
	// 表达式尾节序
	int lastMake;
	// 宏表达式
	QString express;
	// 左操作数
	QString leftOpter;
	// 右操作数
	QString rightOpter;
public:
	// 获取该节点的 xml 映射实体
	dXml::XmlDataDoc getDoc( );
};
#endif // H_MY__MATHEXPRESSBODY_H__H
