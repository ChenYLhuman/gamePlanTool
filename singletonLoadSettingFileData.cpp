﻿#include "singletonLoadSettingFileData.h"
#include "LoadSettingFileData.h"

lThread::LoadSettingFileData* appInstances::LoadSettingFileData::GetSigInstanceInstance(bool deleteFlag, const SigInstance* sigInstance, QWidget* paren) {
	static lThread::LoadSettingFileData* instance = Q_NULLPTR;
	if( deleteFlag && instance ) {
		instance->releaseResources( );
		delete instance;
		instance = Q_NULLPTR;
		return instance;
	}
	if( instance == Q_NULLPTR && !deleteFlag ) {
		instance = new lThread::LoadSettingFileData(sigInstance, paren);
	}
	return instance;
}
