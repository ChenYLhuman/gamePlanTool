﻿#ifndef H_MY__SAPPINSTANCE_H__H
#define	H_MY__SAPPINSTANCE_H__H
#include <QMutexLocker>
#include "IMsgStream.h"
#include "mainwindow.h"
#include "MyApplication.h"
#include "mainwindow.h"
// 单例空间声明
namespace appInstances {
	class PageWidgetS;
	class MsgAppTxt;
	class MsgStream;
	class MsgLocker;
	class LoadDiscDirInfoThread;
	class Menus;
	class DxmlSttringMap;
	class LoadSettingFileData;
};

class appInstances::MsgLocker {
public:
	static QMutex* getMutex( );
};

// 单例空间

#endif // H_MY__SAPPINSTANCE_H__H
