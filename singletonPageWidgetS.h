﻿#ifndef H_MY__SINGLETONPAGEWIDGETS_H__H
#define	H_MY__SINGLETONPAGEWIDGETS_H__H
#include "PageWidgetS.h"

namespace appInstances {
	class PageWidgetS;
}

class appInstances::PageWidgetS {
public:
	static ::PageWidgetS* GetSigInstanceInstance( );
};
#endif // H_MY__SINGLETONPAGEWIDGETS_H__H
