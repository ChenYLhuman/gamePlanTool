﻿#ifndef H_MY__COMMUNICATIONITEM_H__H
#define	H_MY__COMMUNICATIONITEM_H__H
#include <QCheckBox>
#include <QScrollArea>
#include "AAlgorithmItem.h"
#include "AlgorithmTopItemWidgetItem.h"

namespace widgetUI {
	class AlgorithmItem;
}

class widgetUI::AlgorithmItem : public widgetUI::AAlgorithmItem {
Q_OBJECT;
private:
	dXml::XmlDataUnit* xmlDataUnit;
	QVBoxLayout* layout;
	QWidget* widget;
	QScrollArea* scrollArea;
	QString itemName;
	QHash<QString, AlgorithmTopItemWidgetItem*> itemMap;
	//QHash<QCheckBox*, QString> perMap;
	QVector<QString> nameList;
private:
	QString filePath;
public:
	void setItemName(const QString& itemName) {
		this->itemName = itemName;
	}

	virtual ~AlgorithmItem( );
	AlgorithmItem(const QString& itemName
		, const QString& path
		, QWidget* parent = Q_NULLPTR
		, const Qt::WindowFlags& f = Qt::WindowFlags( ));
	void setExpression(const QString& express);
	const QString getItemName( ) override;
	const dXml::XmlDataDoc getXmlData( ) const override;
	const QIcon getIcon( ) override;
	QVBoxLayout* mainLayout( ) override;
	QWidget* mainWidget( ) override;
	void appItem(const QString& name
		, const QString& expree);
	void stateChanged(AlgorithmTopItemWidgetItem* item
		, bool status);
	void stateBtnChanged(AlgorithmTopItemWidgetItem* item
		, int status);
};
#endif // H_MY__COMMUNICATIONITEM_H__H
