﻿#ifndef H_MY__MENUPAGE_H__H
#define	H_MY__MENUPAGE_H__H
#include <QToolBox>
#include <QWidget>
#include "ui_MenuPage.h"
#include "ATabPageBase.h"
#include <qdebug.h>
#include <qapplication.h>
#include <qstyle.h>
#include <QIcon>
#include <qboxlayout.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <qprocess.h>
#include <QProgressBar>
#include <qcombobox.h>
#include <qstring.h>
#include <qicon.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <QToolBox>
#include "dXml.h"
#include "LanguageToolPage.h"
#include "TranslatingToolPage.h"

namespace widgetUI {
	class MenuPage;
}

class widgetUI::MenuPage : public widgetUI::ATabPageBase {
Q_OBJECT;
private:
	// 缓存映射
	QHash<dXml::XmlDataDoc*, QString> cacheMap;
	// 结果映射
	QHash<QString, dXml::XmlDataDoc*> resultMap;
	dXml::XmlDataDoc defautDoc;
	QHBoxLayout* head = Q_NULLPTR;
	QString piData;
	QString ipTag;
	QPushButton* allSaveKeppBtn;
	QHBoxLayout* listBoxHead;
	void initUi( );
	void initConnect( );
	void initDefaultSttingXml( );
	void initDefaultLanguageStting( );
	QHash<AToolBoxPage*, QByteArray*>* pages;
public: // 特殊标签卡
	TranslatingToolPage* translatingToolPage;
	QString translatingFile;
private: // 选项卡 qwidget
	void xmlToTranslatingStting(dXml::XmlDataDoc* doc);
public:
	QLineEdit* fileName;
	void saveFile(bool chickel = false);
	void allSave(bool chickel = false);
	void readListItem(bool chickel = false);
	QString currentFile;
	MenuPage(QWidget* parent = Q_NULLPTR);
	void addListItem(const QString& fileFullPath, const QString& fileName);
	void activation(QObject* obj, int index, EVENTFLAG status) override;
	QString getName( ) override;
	QIcon getIcon( ) override;
	void initLayout(QBoxLayout* layout);
	~MenuPage( );
	void clearListBox( );

	QToolBox* getMainToolbox( ) const {
		return mainToolbox;
	}

	QComboBox* getListBox( ) const {
		return listBox;
	}

	QScrollArea* getAreaBox( ) const {
		return areaBox;
	}

	QPushButton* readBtn = Q_NULLPTR;
	QPushButton* writeBtn = Q_NULLPTR;
private:
	Ui::MenuPage ui;
	QToolBox* mainToolbox = Q_NULLPTR;
	QVBoxLayout* tabPageMainLayout = Q_NULLPTR;
	QComboBox* listBox = Q_NULLPTR;
	QProgressBar* progressBar = Q_NULLPTR;
	QVBoxLayout* mainLayout = Q_NULLPTR;
	QScrollArea* areaBox = Q_NULLPTR;
	QWidget* areaBoxWidget = Q_NULLPTR;
	bool isUpdate = true;
	QHash<int, QString> translatingBuff;
	/*public:
		static MenuPage* getInstance( );*/
public:
	// 选项被选中
	void highlighted(int index);
	// 显示翻译
	const QHash<int, QString>* getTranslating( );
};
#endif // H_MY__MENUPAGE_H__H
