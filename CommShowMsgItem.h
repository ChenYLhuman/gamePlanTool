﻿#pragma once
#include <QWidget>
#include <qicon.h>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <qtextbrowser.h>
#include "CommunicationPage.h"
#include "CommShowMsgItemObjName.h"

class CommShowMsgItem : public QWidget {
Q_OBJECT;
public:
	enum msgType {
		SENDER
		,ACCEPT
	};

	friend class widgetUI::CommunicationPage;
	void setBorwseStatus(QTextBrowser* pTextBrowser);
	void setLabelStatus(QLabel* label);
	explicit CommShowMsgItem(enum msgType msgBodyType, QString& userName, QString& msg, QString& imagePath, QWidget* parent = Q_NULLPTR);
	~CommShowMsgItem( );
	void initMenu(QMenu& msg_meun, QMenu& user_name_menu, QMenu& user_image_menu);
private:
	QLabel* luserMsg;
	QVBoxLayout* mainLayout;
	QLabel* userImage;
	QLabel* luserName;
	QSpacerItem* hSpacar;
	QString imagePath;
	QString msg;
	QString userName;
	msgType msgBodyType;
};
